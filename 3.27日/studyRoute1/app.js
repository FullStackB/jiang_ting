const express = require('express');
const app = express();
// 配置模板引擎
app.engine('html',require('express-art-template'));
app.set('views','views');

const router = require('./route');
app.use(router);

// 端口监听
app.listen(80,()=>{
  console.log('http://127.0.0.1');
})
