module.exports.index = (req, res) => {
  res.render('index.html', {
    title: '首页'
  });
}
module.exports.about = (req, res) => {
  res.render('about.html', {
    title: '关于我们'
  });
}
module.exports.music = (req, res) => {
  res.render('music.html', {
    title: '音乐欣赏'
  });
}