const express = require('express');
const router = express.Router();
const controller = require('../controller')
router.get('/', controller.index);
router.get('/about', controller.about);
router.get('/music', controller.music);
module.exports = router;