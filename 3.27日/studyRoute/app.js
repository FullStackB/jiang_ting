const express = require('express');
const app = express();
app.engine('html',require('express-art-template'));
app.set('views','views');
const router = require('./route');
app.use(router);
app.listen(8000,()=>{
  console.log('http://127.0.0.1:8000');
})