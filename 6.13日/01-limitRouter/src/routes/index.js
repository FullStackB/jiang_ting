// 引入vue
import Vue from 'vue';

// 引入axios
import axios from 'axios';
Vue.prototype.$http = axios;

// 引入路由包
import VueRouter from 'vue-router';
// 使用路由
Vue.use(VueRouter);

// 引入组件
import movieList from '../components/movieList.vue';
import movieDateil from '../components/movieDateil.vue';

// 配置路由
const router = new VueRouter({
  routes:[
    {path:'/',redirect:'/movie/list'},
    {path:'/movie/list',component:movieList},
    {path:'/movie/date/:id',component:movieDateil,props:true},
  ]
})

export default router;