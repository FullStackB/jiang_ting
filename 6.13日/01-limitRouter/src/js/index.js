// 引入vue
import Vue from 'vue';

// 引入组件
import app from '../components/app.vue';

// 引入路由
import router from '../routes/index.js'

// 使用vue 渲染 挂载路由
new Vue({
  el:'#app',
  render:h=>h(app),
  router
})