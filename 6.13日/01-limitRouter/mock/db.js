// 导入lodash
const _ = require('lodash');

// 导入faker 生成假数据
const faker = require('faker');

module.exports = () => {
  const data = { movielist: [] }
  data.movielist = _.times(100, n => {
    return {
      id: n + 1,
      name: faker.random.words(),
      type: faker.random.word(),
      citme: faker.date.recent()
    }
  })
  return data
}
