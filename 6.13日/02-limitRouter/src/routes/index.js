// 引入Vue
import Vue from 'vue';

// 引入vue-router 
import VueRouter from 'vue-router';
// 使用
Vue.use(VueRouter);

// 引入组件
import movieList from '../components/movieList.vue';
import movieDateil from '../components/movieDateil.vue';

// 配置路由规则
const router = new VueRouter({
  routes:[
    {path:'/',redirect:'/movie/list'},
    {path:'/movie/list',component:movieList},
    {path:'/movie/dateil/:id',component:movieDateil,props:true}
  ]
})

export default router;