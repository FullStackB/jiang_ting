// 导入vue
import Vue from 'vue';

// 引入axios
import axios from 'axios';
// 使用
Vue.prototype.$http = axios;

// 引入组件
import app from '../components/app.vue';

// 引入路由
import router from '../routes/index.js'

// 实例化Vue 渲染 挂载
new Vue({
  el:'#app',
  render:h => h(app),
  router
})