const _ = require('lodash');

const faker = require('faker');

module.exports = () => {
  const data = {movielist:[]}

  data.movielist = _.times(10,n => {
    return{
      id:faker.random.uuid(),
      name:faker.random.words(),
      type:faker.random.word(),
      ctime:faker.date.recent()
    }
  })
  return data;
}