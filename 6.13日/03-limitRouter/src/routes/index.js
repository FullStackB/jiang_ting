import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import movieList from '../components/movieList.vue';
import movieDateil from '../components/movieDateil.vue';

const router = new VueRouter({
  routes:[
    {path:'/',redirect:'/movielist'},
    {path:'/movielist',component:movieList},
    {path:'/movieDateil/:id',component:movieDateil,props:true}
  ]
})

export default router;