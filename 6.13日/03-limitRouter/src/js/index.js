import Vue from 'vue';

import axios from 'axios';
Vue.prototype.$http = axios;

import app from '../components/app.vue';

import router from '../routes/index.js'

new Vue({
  el:'#app',
  render:h => h(app),
  router
})