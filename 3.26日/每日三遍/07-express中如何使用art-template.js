// var express = require('express');
// var app = express();
// app.engine('html', require('express-art-template'));
// app.set('views', './views');
// let students = [{
//   name: '张三',
//   age: '18',
//   sex: "男",
//   hobby: '篮球'
// }, {
//   name: '张三1',
//   age: '18',
//   sex: "男",
//   hobby: '养马'
// }, {
//   name: '张三2',
//   age: '18',
//   sex: "男",
//   hobby: '汤头'
// }, {
//   name: '张三3',
//   age: '18',
//   sex: "男",
//   hobby: '抽烟'
// }, {
//   name: '张三4',
//   age: '19',
//   sex: "女",
//   hobby: '篮球'
// }]

// app.get('/', (req, res) => {
//   res.render('students.html', {
//     students: students
//   });
// })
// app.listen(80, () => {
//   console.log('http://127.0.0.1');
// })

// ============================

// var express = require('express');
// var app = express();
// app.engine('html',require('express-art-template'));
// app.set('views','./views');
// let students = [
//   {
//       name: '张三',
//       age: '18',
//       sex: "男",
//       hobby: '篮球'
//     }, {
//       name: '张三1',
//       age: '18',
//       sex: "男",
//       hobby: '养马'
//     }, {
//       name: '张三2',
//       age: '18',
//       sex: "男",
//       hobby: '汤头'
//     }, {
//       name: '张三3',
//       age: '18',
//       sex: "男",
//       hobby: '抽烟'
//     }, {
//       name: '张三4',
//       age: '19',
//       sex: "女",
//       hobby: '篮球'
//     }
// ]
// app.get('/',(req,res) => {
//   res.render('students.html',{students:students});
// })
// app.listen(80,() => {
//   console.log('http://127.0.0.1');
// })

// =====================================

var express = require('express');
var app = express();
app.engine('html', require('express-art-template'));
app.set('views', './views');
let students = [{
  name: '张三',
  age: '18',
  sex: "男",
  hobby: '篮球'
}, {
  name: '张三1',
  age: '18',
  sex: "男",
  hobby: '养马'
}, {
  name: '张三2',
  age: '18',
  sex: "男",
  hobby: '汤头'
}, {
  name: '张三3',
  age: '18',
  sex: "男",
  hobby: '抽烟'
}, {
  name: '张三4',
  age: '19',
  sex: "女",
  hobby: '篮球'
}]
app.get('/', (req, res) => {
  res.render('students.html', {
    students: students
  });
})
app.listen(80, () => {
  console.log('http://127.0.0.1');
})