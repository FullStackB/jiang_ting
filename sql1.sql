-- # 查询学过「张三」老师授课的同学的信息
SELECT id FROM teacher WHERE t_name = '张三';

SELECT c_id FROM course WHERE t_id = (SELECT t_id FROM teacher WHERE t_name = '张三');
-- SELECT * FROM student WHERE s_id = (SELECT s_id FROM score WHERE c_id = )
SELECT * FROM student a LEFT JOIN score b ON a.s_id = b.s_id LEFT JOIN course c ON b.c_id = c.c_id LEFT JOIN teacher d ON c.t_id = d.t_id HAVING d.t_name = '张三';
-- # 查询出只选修两门课程的学生学号和姓名

-- # 查询没有学全所有课程的同学的信息

-- # 查询选修「张三」老师所授课程的学生中，成绩最高的学生信息及其成绩

-- # 取出每科成绩最高的学生信息和科目名称

-- # 显示编号为 01 的课程的分数小于 60 分的学生信息及分值

-- # 查询有两门及两门以上不及格的学生的信息和平均成绩

-- # 显示所有学生的信息及平均成绩

-- # 查询没学过"张三"老师讲授的任一门课程的学生姓名

-- # 查询至少有一门课与学号为" 01 "的同学所学相同的同学的信息