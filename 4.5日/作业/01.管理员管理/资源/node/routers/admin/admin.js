let express = require("express");

let router = new express.Router();

const mysql = require("../../config/db.js");

const crypto = require('crypto');
// 导入moment模块
const moment = require("moment");

// 管理员管理首页
router.get('/', function (req, res, next) {
	// 获取地址栏数据 req.query
	// 获取表单数据 req.body
	// res.send("管理员管理首页");

	// 从数据库中查询数据
	// 判断是否执行成功
	// 加载页面

});

// 管理员管理添加页面
router.get('/add', function (req, res, next) {
	// res.send("管理员管理添加页面");
	// 加载页面
});
// 管理员的添加功能
router.post("/add", function (req, res, next) {
	// 接受数据
	// 判断用户名是否书写
	// 判断用户名长度
	// 判断密码
	// 判断两次密码师傅一直
	// 判断该用户名是否已经注册
	// 判断是否有错误
	// 判断该用户名是否注册
	// 没有注册，我们需要插入数据
	// 当前时间戳
	// 密码加密
	// 判断
	// 判断是否执行成
});

// 无刷新修改状态
router.get("/ajax_status", function (req, res, next) {
	// 接受对应的数据
	// 修改数据
})

// 管理员管理修改页面
router.get('/edit', function (req, res, next) {
	// 接受数据的ID
	// 查询对应数据
	// 判断
	// 加载修改页面
});

// 管理员数据修改功能
router.post("/edit", function (req, res, next) {
	// 接受表单提交的数据
	// 判断该用户是否修改密码
	// 密码加密
	// sql语句
	// sql语句
	// 执行sql语句
});

// 无刷新删除数据
router.get("/ajax_del", function (req, res, next) {
	// 接受地址栏数据
	// 删除数据
	// 判断是否执行成功
})

module.exports = router;