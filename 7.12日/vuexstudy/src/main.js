import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex';
Vue.use(Vuex);
Vue.config.productionTip = false

// 创建store公共状态对象
const store = new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    handleAdd(state, steps) {
      state.count = state.count + steps;
    },
    handleSub(state, steps) {
      state.count = state.count - steps;

    }
  },
  actions:{
    asyncAdd(context,steps){
      setTimeout(() => {
        context.commit("handleAdd",steps)
      }, 100);
    },
    asyncSub(context,steps){
      setTimeout(() => {
        context.commit("handleSub",steps)
      }, 100);
    }
  },
  getters:{
    word(state){
      return "我刚才点了一下按钮 发生 count是:"+state.count;
    }
  }
})

new Vue({
  render: h => h(App),
  store
}).$mount('#app')
