import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// 引入element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

// 引入axios
import Axios from 'axios';
Vue.prototype.$http = Axios;
Axios.defaults.baseURL = "http://127.0.0.1:8888/api/private/v1/"

import TableTree from 'vue-table-with-tree-grid';
Vue.component("table-tree", TableTree);

import VueQuillEditor from 'vue-quill-editor'

// require styles
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'

Vue.use(VueQuillEditor)

Vue.config.productionTip = false;

Vue.filter("dataForm", (originVal) => {
  let date = new Date(originVal);
  let year = date.getFullYear();
  let month = date.getMonth();
  let day = date.getDate();
  let hours = date.getHours();
  let min = date.getMinutes();
  let se = date.getSeconds();
  return `${year}-${month}-${day} ${hours}:${min}:${se}`
})


// 拦截器
Axios.interceptors.request.use(
  function (config) {
    config.headers.Authorization = window.sessionStorage.getItem('token');
    return config
  },
  function (error) {
    return Promise.reject(error);
  }
)

// 引入全局基础css
import './assets/css/base.css'
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
