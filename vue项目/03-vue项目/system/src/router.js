import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/home",
      component: Home,
      children: [
        {
          path: "/home",
          redirect: "/weCome"
        },
        {
          path: "/weCome",
          component: () => import("@/components/weCome.vue")
        },
        {
          path: "/users",
          component: () => import("@/components/UsersList.vue")
        },
        {
          path: "/roles",
          component: () => import("@/components/RolesList.vue")
        },
        {
          path: "/rights",
          component: () => import("@/components/RightsList.vue")
        }, {
          path: "/goods",
          component: () => import("@/components/GoodsList.vue")
        },
        {
          path: "/goods/add",
          component: () => import("@/components/GoodsAddList.vue")
        },
        {
          path: "/orders",
          component: () => import("@/components/OrdersList.vue")
        },
        {
          path: "/params",
          component: () => import("@/components/ParamsList.vue")
        },
        {
          path: "/categories",
          component: () => import("@/components/CategoriesList.vue")

        }
      ]
    },
    {
      path: "/login",
      name: "login",
      component: () =>
        import("./views/Login.vue")
    }
  ]
});

// 路由导航守卫
router.beforeEach((to, from, next) => {
  // 要去的下个页面是登陆 就放行
  if (to.path == "/login") return next();
  // 获取当前浏览器的token
  let tokenStr = window.sessionStorage.getItem('token');
  // 判断当前浏览器没有token
  if (!tokenStr) {
    // 清除浏览器中的token
    window.sessionStorage.removeItem('token');
    // 强制去登录
    next('/login')
  }
  next();
})

export default router;