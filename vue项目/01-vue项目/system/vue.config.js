module.exports = {
  // 控制webpack的配置
  devServer: {
    // 自动打开网页
    open: true,
    // 热更新
    hot: true
  },
  configureWebpack: {
    externals: {
      "vue": "Vue",
      "vue-router": "VueRouter",
      "lodash":"_",
      "axios":"axios",
      "echarts":"echarts"
    }
  }
};
