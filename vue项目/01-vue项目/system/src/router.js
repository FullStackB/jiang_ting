import Vue from "vue";
import VueRouter from "vue-router";
import Home from "./views/Home.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: "/home"
    },
    {
      path: "/home",
      component: Home,
      children: [
        { path: "/home", redirect: "/welcome" },
        {
          path: "/welcome",
          // 懒加载 组件 好处是 用的时候才去引入组件
          component: () => import("@/components/welcome.vue")
        },
        // 用户列表
        {
          path: "/users",
          component: () => import("@/components/UsersList.vue")
        },
        {
          path: "/rights",
          component: () => import("@/components/rightsList.vue")
        },
        {
          path: "/roles",
          component: () => import("@/components/RolesList.vue")
        },
        {
          path: "/categories",
          component: () => import("@/components/categoryList.vue")
        },
        {
          path: "/params",
          component: () => import("@/components/ParamsList.vue")
        },
        {
          path: "/orders",
          component: () => import("@/components/ordersList.vue")
        },
        {
          path: "/goods",
          component: () => import("@/components/GoodsList.vue")
        },
        {
          path: "/goods/add",
          component: () => import("@/components/GoodsAdd.vue")
        },
        {
          path: "/reports",
          component: () => import("@/components/DataReports.vue")
        }
      ]
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./views/Login.vue")
    }
  ]
});

// 路由导航守卫
router.beforeEach((to, from, next) => {
  // 判断是不是去登录 如果是放行
  if (to.path == "/login") return next();
  // 获取浏览器中保存的token令牌
  let tokenStr = window.sessionStorage.getItem("token");
  // 判断是否登录
  if (!tokenStr) {
    // 清除浏览器中的token令牌
    window.sessionStorage.clear();
    // 没登录去登录
    next("/login");
  }
  // 放行
  next();
});

export default router;
