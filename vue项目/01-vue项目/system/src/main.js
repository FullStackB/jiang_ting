import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// // 引入element-ui
// import ElementUI from "element-ui";
// import "element-ui/lib/theme-chalk/index.css";
// Vue.use(ElementUI);

Vue.config.productionTip = false;

import tableTree from "vue-table-with-tree-grid";
Vue.component("table-tree", tableTree);

// 引入axios 配置
import axios from "axios";
Vue.prototype.$http = axios;
axios.defaults.baseURL = "https://www.liulongbin.top:8888/api/private/v1/";

import VueQuillEditor from "vue-quill-editor";
import "quill/dist/quill.core.css";
import "quill/dist/quill.snow.css";
import "quill/dist/quill.bubble.css";
Vue.use(VueQuillEditor);

// 引入全局基础css
import "./assets/css/base.css";

// 请求拦截器
axios.interceptors.request.use(
  function(config) {
    config.headers.Authorization = window.sessionStorage.getItem("token");
    return config;
  },
  function(error) {
    return Promise.reject(error);
  }
);

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
