import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      redirect: "/home"
    },
    {
      path: "/home",
      component: Home,
      children: [
        {
          path: '/home',
          redirect: '/welcome'
        }
        , {
          path: "/welcome",
          component: () => import("@/components/weCome.vue")
        },
        {
          path: '/users',
          component: () => import("@/components/usersList.vue")
        },
        {
          path: '/roles',
          component: () => import("@/components/rolesList.vue")
        },
        {
          path: '/rights',
          component: () => import("@/components/rightsList.vue")
        },
        {
          path: '/goods',
          component: () => import("@/components/goodsList.vue")
        },
        {
          path: '/goods/add',
          component: () => import("@/components/goodsAddList.vue")
        },
        {
          path: '/params',
          component: () => import("@/components/paramsList.vue")
        },
        {
          path: "/categories",
          component: () => import("@/components/categoriesList.vue")
        },{
          path:"/orders",
          component: () => import("@/components/ordersList.vue")
        },
        {
          path:"/reports",
          component: () => import("@/components/reportsList.vue")

        }
      ]
    },
    {
      path: "/login",
      name: "login",
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () =>
        import(/* webpackChunkName: "about" */ "./views/Login.vue")
    }
  ]
});

// 路由导航守卫
router.beforeEach((to, from, next) => {
  // 判断要去的那个路径是不是登录页面 如果是就放行
  if (to.path == "/login") return next();
  // 获取当前浏览器的token
  let tokenStr = window.sessionStorage.getItem('token');
  // 判断有没有token
  if (!tokenStr) {
    // 没有的话清除浏览器token
    window.sessionStorage.removeItem('token');
    // 去登陆
    next('/login')
  }
  // 放行
  next();
})

export default router;