import Vue from "vue";
import App from "./App.vue";
import router from "./router";

// 引入element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI);

// 引入axios
import axios from 'axios';
Vue.prototype.$http = axios;
axios.defaults.baseURL = "http://127.0.0.1:8888/api/private/v1/";

// 引入表格组件
import tableTree from 'vue-table-with-tree-grid'
Vue.component("table-tree", tableTree);


// 引入 vue-quill-editor
import VueQuillEditor from 'vue-quill-editor';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';
Vue.use(VueQuillEditor)

Vue.config.productionTip = false;

// 引入全局基础css
// import './assets/css/base.css'
import './assets/css/base.css';

// 请求拦截器
axios.interceptors.request.use(
  function (config) {
    config.headers.Authorization = window.sessionStorage.getItem('token');
    return config
  },
  function (error) {
    return Promise.reject(error);
  }
)

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
