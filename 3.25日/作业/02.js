// 导入express
const express = require('express');
// 创建服务器
const app = express();
// 导入querystring
const qs = require('querystring');
// 导入fs
const fs = require('fs');
// 导入path
const path = require('path');
// 静态资源托管
app.use(express.static('public'));
// 监听
app.get('/login', (req, res) => {
  // 获取注册页面的内容
  var str = req.query;
  // 转换字符串
  var obj = JSON.stringify(str);
  // 将获取到的内容写入logi1n.json中
  fs.writeFile(path.join(__dirname + '/login.json'), obj, 'utf8', (err) => {})
  // 读取登录页面 实现跳出转
  res.send(fs.readFileSync(path.join(__dirname, "./public/logon.html"), "utf-8"))
})

// 监听
app.get('/logon', (req, res) => {
  // 获取登录页面的内容
  var str1 = req.query;
  // 获取注册页面的内容
  var log = fs.readFileSync(path.join(__dirname, '/login.json'), 'utf8');
  // 转换对象
  var a = JSON.parse(log);
  // 判断两个页面的用户名和密码是否一致
  if (str1.usename == a.usename && str1.password == a.password) {
    // 成功就跳转到欢迎的页面
    res.send(fs.readFileSync(path.join(__dirname, './public/center.html'), 'utf8'));
  }
  // console.log(str1);
  // console.log(a);
})
// 监听端口
app.listen(80, () => {
  console.log('http://127.0.0.1')
})