// 导入fs
const fs = require('fs');
// 导入path
const path = require('path');

let ea = `<!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Document</title>
    </head>
    <body>
      我叫王大锤,今年18岁,我的特点是能吃能喝能睡
    </body>
    </html>`;
    // 同步创建
var reslut = fs.writeFileSync(path.join(__dirname + '/exam.html'), ea, 'utf8', (err) => {
})

// 导入http
const http = require('http');
// 创建服务器
const server = http.createServer();
// 请求监听
server.on('request', (req, res) => {
  // 防止乱码
  res.writeHead(200, {
    'Content-Type': 'text/html;charset=UTF-8'
  });

  if (req.url == '/') {
    res.end('欢迎光临')
  } else if (req.url == '/exam.html') {
    fs.readFile(path.join(__dirname + '/exam.html'), 'utf8', (err, data) => {
      if (err) {
        err.message;
      }
      res.end(data);
    })
  } else {
    res.end('404,页面找不到');
  }
})

// 启动监听
server.listen(7878, () => {
  console.log('请访问:http://127.0.0.1:7878');
})