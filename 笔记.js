// IN关键字：IN(xx，yy，...) 满足条件范围内的一个值即为匹配项

// between...and:在...到...之间的值都为匹配项

// like:相当于模糊查询,和like一起使用的通配符有 "%", "_"
      // "%":能匹配任意长度的字符
      // "_":只能表示任意一个字符

// and:相当于逻辑与,要同时满足条件

// or:逻辑或,只需要满足一个条件就可以匹配上

// distinct:去掉重复数据,使查询不重复

// order by:排序
      // asc:升序(默认)
      // desc:降序

// group by:分组
      // 知道GROUP BY的意义，并且会使用HAVING对分组进行过滤， HAVING和WHERE都是进行条件过滤的，区别就在于 WHERE 是在分组之前进行过滤，而HAVING是在分组之后进行条件过滤。

// 内连接：表名 inner join 表名 on 连接条件

// 