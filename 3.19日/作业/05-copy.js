let fs = require('fs');

// 写入
fs.writeFile('05-copy.txt', '我准备拷贝文件', 'utf8', (err) => {
  console.log('写入成功');

})
// 追加
fs.appendFile('05-copy.txt', '我继续追加信息到文件中', (err) => {
  console.log('追加成功');

})

// 获取
fs.stat('05-copy.txt', (err, stats) => {
  console.log('获取失败');
  // 获取大小
  console.log(stats.size); //0
  // 获取创建时间
  console.log(stats.atime); //2019-03-19T10:25:55.722Z
  // 判断是不是一个文件
  console.log(stats.isFile()); //true
  // 判断是不是目录
  console.log(stats.isDirectory()); //false
})

// 添加延时定时器
setTimeout(function () {
  // 复制
  fs.copyFile(__dirname + '/05-copy.txt', __dirname + '/05-copyFile.txt', 'utf8', (err) => {
    console.log(err);

  })
}, 2000);