const fs = require('fs');
// 写入
fs.writeFile('04-stat.txt', '全栈谁最牛', (err) => {
  console.log('写入成功')
})
// 追加
fs.appendFile('04-stat.txt', '我最牛', (err) => {
  console.log('追加成功')
})
fs.stat('04-stat.txt', (error, stats) => {
  // 获取新单位字节  
  console.log(stats.size); //0
  // 获取创建时间
  console.log(stats.atime); //2019-03-19T10:25:55.717Z
  // 判断是否一个文件
  console.log(stats.isFile()); //true
  // 判断是不是一个目录
  console.log(stats.isDirectory()); //false
})