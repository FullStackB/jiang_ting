// const声明的常量不能改变
// const pi = 3.143232;
// pi = 3920;
// console.log(pi);


// const声明的常量必须赋值
// const pi;
// pi = 312323;

// const不能重复声明常量
// const pi = 2;
// const pi = 2;

// const的常量值真得不能改变吗
// const obj = {
//   name:'蒋婷'
// }
// obj.sex = '女';
// console.log(obj);