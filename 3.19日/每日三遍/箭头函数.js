// 第一遍
// var fn = (x,y) =>{
//   console.log(x + y);
// }
// fn(3,4);

// let fn = (a,b) =>{
//   console.log(a - b);
// }
// fn(43,4)

// 第二遍
// var fn = (x,y) => {
//   console.log(x + y);
// }
// fn(4,5)
// let fn = (x,y) => {
//   console.log(x + y);
// }
// fn(90,0)

// 第三遍
// var fn = (x,y) => {
//   console.log(x + y);
// }
// fn(9,5)
// let fn = (x,y) => {
//   console.log(x + y);
// }
// fn(9,0)