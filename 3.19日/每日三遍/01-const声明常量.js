// const声明的常量不能改变
// const PI = 3.1415926;
// PI = 2;
// console.log(PI);

// const声明的常量必须赋值
// const PI;
// PI = 3.1415;

// const不能重复声明常量
// const pi = 1;
// const pi = 1;

// const的常量值真得不能改变吗
// var obj = {
//   name: 'zhangsan'
// }
// obj.age = 18;
// console.log(obj);

// const obj = {
//   name:'蒋婷'
// }
// obj.age = 20;
// console.log(obj);
