// 内层变量可能会覆盖外层变量
// var age = 10;

// function fn() {
//   console.log(age);
//   if (false) {
//     var age = 'hello';
//   }
// }
// fn();

// var age = 10;
// function fn(){
//   console.log(age);
//   if(false){
//     let age = 'hello';
//   }
// }
// fn();

// 用来计数的循环变量泄露为全局变量
// for(var i = 0;i < 10;i++){

// }
// var i = 100;
// console.log(i);

// for(let i = 10;i < 10;i++){

// }
// console.log(i); //i is not defined