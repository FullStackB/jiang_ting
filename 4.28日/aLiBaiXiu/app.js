// 导入express框架
const express = require('express');
// 创建服务器
const app = express();
// 设置静态托管目录
app.use(express.static('public'));
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));
// 导入ejs包
const ejs = require('ejs');
// 设置模板引擎的后缀
app.set('view engine','ejs');
// 设置模板引擎使用的模板的路径
app.set('views','./views');

// 导入路由模块
const usersrouter = require('./router/usersRouter');
// 挂载路由
app.use(usersrouter);

// 创建端口
app.listen(80,()=>{
  console.log('http://127.0.0.1');
})
