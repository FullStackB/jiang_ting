const conn = require('../data');

module.exports.beLogin = (req,res) => {
  res.render('login');
}
module.exports.beIndex = (req,res) => {
  res.render('index')
}
// 文章管理页面
module.exports.bePosts = (req, res) => {
  res.render('posts');
}

// 文章的新增
module.exports.beAddPosts = (req, res) => {
  res.render('post-add');
}

// 分类目录管理
module.exports.beCategories = (req, res) => {
  res.render('categories');
}

// 评论管理
module.exports.beComments = (req, res) => {
  res.render('comments');
}

// 用户管理
module.exports.beUsers = (req, res) => {
  res.render('users');
}
module.exports.usersFind = (req, res) => {
  conn.query('select * from users', (err, results) => {
    if (err) {
      return console.log(err);
    }
    res.json(results);
  })
}


// 添加用户
module.exports.userAdd = (req, res) => {
  console.log(req.body);
  let sqlDate = ['/uploads/avatar_1.jpg', req.body.email, req.body.slug, req.body.nickname, req.body.password, 'activated'];
  let sql = 'insert into users values (null, ?,?,?,?,?,?);'

  conn.query(sql, sqlDate, (error, results) => {
    if (error) {
      return console.log(error);
    }

    if (results.affectedRows) {
      res.json({
        code: '1000',
        message: '用户添加成功'
      })
    }
  })
}

// // 删除用户
module.exports.usersDelete = (req, res) => {
  console.log(req.query.id);
  conn.query('delete from users where id=?', req.query.id, (error, results) => {
    if (error) {
      return console.log(error);
    }else{
      res.json({
        code: '1001',
        message: '用户删除成功'
      })
    }

  })
}


// 导航设置
module.exports.beNav = (req, res) => {
  res.render('nav-menus');
}

// 轮播图管理
module.exports.beSlides = (req, res) => {
  res.render('slides');
}

// 网站设置
module.exports.beSettings = (req, res) => {
  res.render('settings');
}


// 个人中心
module.exports.beProfile = (req, res) => {
  res.render('profile');
}