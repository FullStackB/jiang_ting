const conn = require('../data');
// 用户管理
module.exports.beUsers = (req, res) => {
  res.render('users');
}
module.exports.usersFind = (req, res) => {
  conn.query('select * from users', (err, results) => {
    if (err) {
      return console.log(err);
    }
    res.json(results);
  })
}

// 添加用户
module.exports.userAdd = (req, res) => {
  console.log(req.body);
  let sqlDate = ['/uploads/avatar_1.jpg', req.body.email, req.body.slug, req.body.nickname, req.body.password, 'activated'];
  let sql = 'insert into users values (null, ?,?,?,?,?,?);'

  conn.query(sql, sqlDate, (error, results) => {
    if (error) {
      return console.log(error);
    }

    if (results.affectedRows) {
      res.json({
        code: '1000',
        message: '用户添加成功'
      })
    }
  })
}

// 删除用户
module.exports.usersDelete = (req, res) => {
  console.log(req.query.id);
  conn.query('delete from users where id=?', req.query.id, (error, results) => {
    if (error) {
      return console.log(error);
    }else{
      res.json({
        code: '1001',
        message: '用户删除成功'
      })
    }
  })
}

$('table').on('click','')