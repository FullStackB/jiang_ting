// 引入express
const express =  require('express');
// 使用express的router来创建路由
const router = express.Router();
// 导入控制器模块
const controller = require('../controller/usersCtrl');
// 4.7 配置用户管理页面
router.get('/beusers', controller.beUsers);
// 用户查询
router.get('/usersFind',controller.usersFind);
// 4.7.2 用户添加
router.post('/userAdd', controller.userAdd);
// // 4.7.3 用户删除
router.get('/usersDelete', controller.usersDelete);