// 引入express
const express =  require('express');
// 使用express的router来创建路由
const router = express.Router();
// 导入控制器模块
const controller = require('../controller');

// 登录页面显示
router.get('/belogin',controller.beLogin);
// 配置首页面显示
router.get('/beindex',controller.beIndex);
// 4.3 配置文章管理页面
router.get('/beposts', controller.bePosts);
// 4.4 配置增加(写)文章页面
router.get('/beaddposts', controller.beAddPosts);
// 4.5 配置分类目录页面
router.get('/becategories', controller.beCategories);
// 4.6 配置评论管理页面
router.get('/becomments', controller.beComments);
// 4.7 配置用户管理页面
router.get('/beusers', controller.beUsers);
// 用户查询
router.get('/usersFind',controller.usersFind);
// 4.7.2 用户添加
router.post('/userAdd', controller.userAdd);
// // 4.7.3 用户删除
router.get('/usersDelete', controller.usersDelete);
// 4.8 配置导航设置页面
router.get('/benav', controller.beNav);
// 4.9 配置图片轮播管理页面
router.get('/beslides', controller.beSlides);
// 4.10 网站设置页面
router.get('/besettings', controller.beSettings);
// 4.11 个人中心
router.get('/beprofile', controller.beProfile)
module.exports = router;