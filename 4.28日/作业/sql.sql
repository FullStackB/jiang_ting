单表基础查询
1)	统计订购了商品的总人数。
select count(cid) '购了商品的总人数' from orderItem ;

2)	统计顾客号和所订购商品总数量
   select cid , sum(count) from orderItem group by cid;

子查询：
1)	查找没订购商品的顾客号和顾客名。
select cid from orderItem ;

select cid,cname from customer where cid not in(select cid from orderItem );
2)	查找订购商品号'0001'商品数量最多的顾客号和顾客名
select max(count) from orderItem where pid ='p001';  
select cid from orderItem where count=(select max(count) from orderItem where pid ='p001') and pid='p001';
select cid , cname from customer where cid in (select cid from orderItem where count=(select max(count) from orderItem where pid ='p001') and pid='p001');
3)	统计至少订购2种商品的顾客id和顾客名。
-- ?#### 9) 查询至少选修了2门课程的学生学号
-- select  sid, count(*) from sc group by cid having count(*)>1;
select cid from orderItem group by cid having count(pid)>1;
select cid,cname from customer where cid in (select cid from orderItem group by cid having count(pid)>1);
自连接
Bbdfr
左右连接
1)	查找所有顾客号和顾客名以及他们购买的商品号
```
select a.cid ,a.cname ,b.pid from customer a right join orderItem b on a.cid=b.cid ;
函数应用
OI0
多表查询
1)	查找订购了商品"p001"的顾客号和顾客名。
select a.cid, b.cname ,a.pid from customer b, orderItem a where a.cid=b.cid and a.pid='p001';
2)	查找订购了商品号为"p001"或者"p002"的顾客号和顾客名。
select a.cid, b.cname ,a.pid from customer b, orderItem a where a.cid=b.cid and a.pid='p001' or a.pid='p002';
3)	查找年龄在30至40岁的顾客所购买的商品名及商品单价。
select a.pname,a.pirce from product a,customer b, orderItem c where a.pid = c.pid and b.cid =c.cid and b.age > 30 and b.age < 40;
4)	查找女顾客购买的商品号，商品名和价格。
select a.pid,a.pname,a.pirce from product a,customer b, orderItem c where a.pid = c.pid and b.cid =c.cid and b.sex='女';

