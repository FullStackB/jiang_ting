const add = require('./lib/add');
const chu = require('./lib/chu');
const chen = require('./lib/chen');
const minu = require('./lib/minu');

module.exports ={
  add,
  chu,
  chen,
  minu
}