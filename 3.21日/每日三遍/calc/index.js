const add = require('./lib/add');
const minus = require('./lib/minus');

module.exports = {
  add,
  minus
}