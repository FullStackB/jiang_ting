// 第一遍
// var jsonArr = "[1,3,4,56,6]";
// var jsonObj = '{"name":"张三","age":32}';
// // JSON.parse把字符串变成对象或数组
// console.log(typeof JSON.parse(jsonArr));
// console.log(typeof JSON.parse(jsonObj));

// var jsonArr = [3, 42, 54, 6];
// var jsonObj = {
//   name: 'zhangsan',
//   age: 21
// }
// // JSON.stringify 把对象或数组变成字符串
// console.log(JSON.stringify(jsonArr));
// console.log(JSON.stringify(jsonObj));
// console.log(typeof JSON.stringify(jsonArr));
// console.log(typeof JSON.stringify(jsonObj));



//第二遍 
// var jsonArr = "[32,65,76,87]";
// var jsonObj = '{"name":"张三","age":321}';
// var arr = JSON.parse(jsonArr);
// console.log(arr);
// var obj = JSON.parse(jsonObj);
// console.log(obj);

// var jsonArr = [32, 65, 76, 87];
// var jsonObj = {
//   name: "张三",
//   age: 321
// };
// var arr1 = JSON.stringify(jsonArr);
// console.log(arr1);
// var obj1 = JSON.stringify(jsonObj);
// console.log(obj1);


// 第三遍
// var jsonArr = "[32,65,76,87]";
// var jsonObj = '{"name":"张三","age":321}';
// var arr = JSON.parse(jsonArr);
// console.log(arr);
// var obj = JSON.parse(jsonObj);
// console.log(obj);

// var jsonArr = [32, 65, 76, 87];
// var jsonObj = {
//   name: "张三",
//   age: 321
// };
// var arr1 = JSON.stringify(jsonArr);
// console.log(arr1);
// var obj1 = JSON.stringify(jsonObj);
// console.log(obj1);