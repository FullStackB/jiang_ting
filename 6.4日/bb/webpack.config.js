const path = require('path');
const HtmlPlugin = require('html-webpack-plugin');
const htmlplugin = new HtmlPlugin({
  template:path.join(__dirname,'./src/index.html'),
  filename:'index.html'
})
module.exports = {
  mode:'development',
  plugins:[htmlplugin],
  entry:path.join(__dirname,'./src/index.js'),
  output:{
    path:path.join(__dirname,'./dist'),
    filename:'main.js'
  }
}