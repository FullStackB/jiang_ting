import Vue from './vue';
import axios from './axios';

Vue.filter('str',function(originVal){
  let date = new Date(originVal);
  let year = date.getFullYear();
  let month = date.getMonth() + 1;
  let day = date.getDate();
  let xs = date.getHours();
  let fz = date.getMinutes();
  let mz = date.getSeconds();
  return `${year}-${month}-${day} ${xs}:${fz}:${mz}`;
})

const vm = new Vue({
  el:'#app',
  data:{
    id:'',
    name:'',
    keywords:'',
    brandList:[]
  }, 
  created:function(){
    this.getInfo();
  },
  methods:{
    getInfo:async function(){
      let {data:res} = await axios.get('http://www.liulongbin.top:3005/api/getprodlist');
      // console.log(result);
      this.brandList = res.message;
    },
    add:async function(){
      if(!(this.name)) return alert('姓名不能为空');
      let name = this.name;
      let {data:res} = await axios.post('http://www.liulongbin.top:3005/api/addproduct',{
        name:name
      });
      if(res.status == 0){
        this.getInfo();
      }
    },
    remove:async function(id){
      let {data:res} = await axios.get('http://www.liulongbin.top:3005/api/delproduct/' + id);
      if(res.status == 0){
        this.getInfo();
      }
    },
    search:function(){
      return this.brandList.filter(item=>item.name.includes(this.keywords));
    }
  }
})