// 引入express框架
const express = require('express');
// 创建服务器
const app = express();
// 引入body-parser包
const bodyParser = require('body-parser');
// 编码格式
app.use(bodyParser.urlencoded({
  extended: false
}));
// 配置模板
app.engine('html', require('express-art-template'));
// 设置文件路径
app.set('views', 'views');
// 设置托管静态资源
app.use('/public', express.static('public'));
// 请求 / 返回get文件
app.get('/', (req, res) => {
  res.sendFile('./views/post.html', {
    root: __dirname
  });
})
// 接受/user返回的值
app.post('/user', (req, res) => {
  res.send(req.body);
})
app.get('/user', (req, res) => {
  res.send(req.query.username);
})

// 启动端口监听
app.listen(80, () => {
  console.log('http://127.0.0.1');
})