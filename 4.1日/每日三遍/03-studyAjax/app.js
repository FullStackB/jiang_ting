const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
  extended: false
}));
app.engine('html', require('express-art-template'));
app.set('views', 'views');
app.use('/public', express.static('public'));
app.get('/', (req, res) => {
  res.sendFile('./views/post.html', {
    root: __dirname
  });
})
app.post('/user', (req, res) => {
  res.send(req.body);
})

app.listen(3030, () => {
  console.log('http://127.0.0.1:3030')
})