// 导入express框架
const express = require('express');
// 创建服务器
const app = express();
// 导入body-parser包
const bodyParser = require('body-parser');
// 设置编码格式
app.use(bodyParser.urlencoded({extended:false}));
// 设置渲染环境
app.engine('html',require('express-art-template'));
// 设置渲染路径
app.set('views','./views');




// 设置静态资源托管
app.use('/public',express.static('public'));

// 导入路由
const rouer=require("./router");
app.use(rouer)

app.listen(80,()=>{
  console.log('http://127.0.0.1');
})