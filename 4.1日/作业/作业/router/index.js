// 导入express的框架
const experss = require('express');
// 使用express的方法
const rouer = experss.Router();
// 导入controller
const controller = require('../controller');
//首页  请求监听
rouer.get('/',controller.index);
// 注册页面
rouer.post('/add',controller.add);
// 管理员页面
rouer.get('/show',controller.show);
// 删除
rouer.get('/deluser',controller.deluser);

rouer.get('/xg',controller.xg);

rouer.get('/modify',controller.modify);

// rouer.get("/show1",controller.show1)
// 暴露出去
module.exports = rouer;