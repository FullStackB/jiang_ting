-- 连接服务器 mysql -u账号 -p密码
mysql -uroot -p123456

--  进入数据库  use 数据库名称;
use czxy;

-- 创建表stu  create table 表名(字段名 字段属性(值),字段名 字段属性(值),字段名 字段属性(值)....);
create table stu(id int,name varchar(255),age int,sex varchar(255),hometown varchar(255),tel int,room_num int) charset = gbk;

-- 添加数据 insert into 表名 values (值)
insert into stu values(1,'蒋婷','18','女','湖南',17380263757,622), (2,'常可欣','20','女','山东',4792420932,622),(3,'张冰雪','19','女','河北',4792420932,622),(4,'巴木子','19','女','湖北',4792420932,622),(5,'张乃赫','19','男','长春',4792420932,411),(6,'小明','18','女','山东',4792420932,622),(7,'小天','18','男','湖北',44332420932,201),
(8,'蒋婷','18','女','湖南',17380263757,622),(9,'和尚','18','男','湖南',17380263757,622),(10,'蒋婷','18','女','湖南',17380263757,622),(11,'蒋婷','18','女','湖南',17380263757,622),(12,'蒋婷','18','女','湖南',17380263757,622),
(13,'蒋婷','18','女','湖南',17380263757,622),(14,'蒋婷','18','女','湖南',17380263757,622),(15,'粉丝','18','男','湖南',17380263757,622),(16,'蒋婷','18','女','湖南',17380263757,622),(17,'蒋婷','18','女','湖南',17380263757,622),
(18,'蒋婷','18','女','湖南',17380263757,622),(19,'蒋婷','18','女','湖南',17380263757,622),(20,'蒋婷','18','女','湖南',17380263757,622);

-- 查询山东的同学  select id,name,age,sex,hometown,tel,room_num from 表名 where hometown = 值;
 select id,name,age,sex,hometown,tel,room_num from stu where hometown = '山东';

--  查询男生有哪些 
select id,name,age,sex,hometown,tel,room_num from stu where sex = '男';

-- 把手机号码是xxx的性别更新为女v
update stu set tel = '17380263757' where sex = '女';

-- 删除id为10和15的同学
delete from stu where id = 10;
delete from stu where id = 15;