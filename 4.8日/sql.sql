-- 登录连接数据库 
mysql -uroot -p123456;

-- 数据库创建  create database 数据库名称 charset = 字符集编码
create database czxy charset = utf8;

-- 删除数据库   drop database 数据库名称
drop database czxy;

-- 创建数据表
create table czxy.user(id int(3), username varchar(255),sex varchar(255),Email varchar(255),phong varchar(255),resume varchar(255));
create table czxy.news(id int(2),title varchar(255),outline varchar(255),content varchar(255),issue varchar(255),click int(3));

-- 删除数据表 
drop table czxy.user;

-- 增加字段
alter table czxy.news add collect int(3);

-- 修改字段
alter table czxy.news change title headline varchar(255);

-- 删除字段
alter table czxy.user drop id;

-- 插入两条数据
insert into czxy.user(username,sex,Email,phong,resume) values ('蒋婷','女','2606938203@qq.com','17380263757','简历');
insert into czxy.news(id,headline,outline,content,issue,click,collect) values (1,'新闻标题','新闻概要','新闻内容','新闻发布事件',243,344);

-- 删除数据 
delete from czxy.news where id =  1;