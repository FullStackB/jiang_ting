const fs = require('fs');

function readFileByPath(fpath) {
  return new Promise(function (resolve, reject) {
    fs.readFile(fpath, 'utf8', (error, data) => {
      if (error) return reject(error);
      resolve(data)
    })
  })
}

readFileByPath('./file/1.txt').then(function (result) {
  console.log(result);
  return readFileByPath('./file/2.txt');
}).then(function (result) {
  console.log(result);
  return readFileByPath('./file/3.txt');
}).then(function (result) {
  console.log(result);
})