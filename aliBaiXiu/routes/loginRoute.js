const express = require('express');
const router = express.Router();
const loginCtrl = require('../controllers/loginCtrl');
router.get('/login',loginCtrl.loginShow);
router.post('/login',loginCtrl.loginUser);
router.get('/loginout',loginCtrl.loginOut)

module.exports = router;