const express = require('express');
const multer = require('multer');
const upload = multer({});
const router = express.Router();

const profileCtrl = require('../controllers/profileCtrl');
router.get('/profile',profileCtrl.profile);
router.post('/profileUplod',upload.single('avatar'),profileCtrl.profileUplod);
module.exports = router;