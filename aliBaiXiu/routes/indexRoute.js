const express = require('express');
const router = express.Router();
const indexCtrl = require('../controllers/indexCtrl');
router.get('/index',indexCtrl.indexShow);
module.exports = router;