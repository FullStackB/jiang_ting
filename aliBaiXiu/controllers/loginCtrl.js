const conn = require('../data');

module.exports.loginShow = (req,res) => {
  res.render('login');
}

module.exports.loginUser = (req,res) => {
  let params = [req.body.email,req.body.password];
  console.log(params);
  conn.query('select * from users where email = ? and password =?',params,(error,result)=>{
    if(error) return console.log(error);
    if(result.length == 0){
      res.json({
        code: '1001',
        msg: '账号或者密码错误'
      })
    }else if(result.length != 0){
      req.session.isLogin = true;
      req.session.user = result[0];
      res.json({
        code: '1002',
        msg:'登录成功'
      })
    }
  })
}

// 退出
module.exports.loginOut = (req,res) => {
  req.session.destroy(function(error){
    if(error){
      return console.log(error)
    }
    res.redirect('/login');
  })
}