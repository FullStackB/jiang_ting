const conn = require('../data');
// #region 显示分类页面
module.exports.cateShow = (req, res) => {
  if (!req.session.isLogin) {
    res.redirect('/login');
    return;
  }
  res.render('categories');
}
// #endregion

// #region 显示分类列表
module.exports.catesFind = (req, res) => {
  conn.query('select * from ali_cate', (error, results) => {
    if (error) return console.log(error);

    res.json(results);
  })
}
// #endregion

// #region 添加分类
module.exports.cateAdd = (req, res) => {
  console.log(req.body);
  let params = [req.body.name, req.body.slug];
  conn.query('insert into ali_cate(cate_name, cate_slug) values (?,?)', params, (error, results) => {
    if (error) return console.log(error);

    if (results.affectedRows) {
      res.json({
        code: '1101',
        message: '分类添加成功'
      })
    }
  })
}
// #endregion

// #region 删除分类
module.exports.cateDelete = (req, res) => {
  conn.query('delete from ali_cate where cate_id=?', req.query.id, (error, results) => {
    if (error) return console.log(error);

    if (results.affectedRows) {
      res.json({
        code: '1102',
        message: '单个分类删除成功'
      })
    }
  })
}
// #endregion

// #region 查询分类信息(一个)
module.exports.cateFind = (req, res) => {
  // console.log(req.query);
  conn.query('select * from ali_cate where cate_id=?', [req.query.id], (error, results) => {
    if (error) return console.log(error);

    res.json(results);
  })
}

module.exports.cateUpdate = (req, res) => {
  let params = [req.body.name, req.body.slug, req.body.id];
  conn.query('update ali_cate set cate_name=?,cate_slug=? where cate_id=?', params, (error, results) => {
    if (error) { return console.log(error) };
    if (results.affectedRows) {
      res.json({
        code: '1103',
        message: '分类目录信息更新成功'
      })
    }
  })
}
// #endregion


// #region 批量删除
module.exports.catesDelete = (req, res) => {
  conn.query('delete from ali_cate where cate_id in(?)', [req.query.idArr], (error, results) => {
    if (error) {
      return console.log(error);
    }

    if (results.affectedRows) {
      res.json({
        code: '1104',
        message: '批量删除成功'
      })
    }
  })
}
// #endregion