const conn = require('../data');
const fs = require('fs');
module.exports.profile = (req,res) => {
  // res.render('profile');
  if(!req.session.isLogin){
    res.redirect('/login');
    return;
  }
  conn.query('select * from users where id =?',[req.session.user.id],(error,result)=>{
    res.render('profile',result[0]);
  })
}

module.exports.profileUplod = (req,res) =>{
  fs.writeFile('./public/uploads/' + req.file.originalname,req.file.buffer,(error) => {
    if(error){
      return console.log(error);
    }
    res.json({
      pic:'./uploads/' + req.file.originalname
    })
  })
}
