module.exports.indexShow = (req,res) => {
  if(!req.session.isLogin){
    res.redirect('/login');
    return;
  }
  res.render('index');
}