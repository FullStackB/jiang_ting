-- 创建一张表
create table ysf_ss(
  int_1 int,
  int_2 int,
  int_3 int,
  int_4 int
);

-- 插入数据
insert into ysf_ss values(1,2,3,4);

-- 使用算术运算符
select int_1+int_2,int_3-int_2,int_2*int_4,int_4/int_2,int_1%int_2 from ysf_ss;