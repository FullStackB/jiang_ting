-- 显示 id为1 3 5 6  的用户
select * from having_data where id in(1,3,5,6);

-- 删除name为小乔 扁鹊这几个人
delete from having_data where name in('小乔','扁鹊');

-- 找出having_data中age不为null的数据
select * from having_data where age is not null;

-- 创建表
create table is_data(
  int_1 int,
  int_2 int
);
-- 插入数据
insert into is_data values
(null,10),
(100,200);

-- 找到int_1为null的数据
select * from is_data where int_1 is null;

select * from having_data where name like '妲%';

select * from having_data where name like '黄_';