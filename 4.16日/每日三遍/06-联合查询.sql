-- 创建表
create table union_data(
  stu_id varchar(5) primary key,
  stu_name varchar(3) not null,
  stu_age tinyint not null default 18,
  stu_height varchar(4) not null,
  stu_set enum('男','女','保密') default '保密'
);

-- 添加数据
insert into union_data values
('stu01','小红',default,'156','女'),
('stu02','小绿',23,'154','男'),
('stu03', '小黑',20, '158','女'),
('stu04', '小白',17, '168','男'),
('stu05', '小婷',16, '175','女'),
('stu06', '小雅',25, '170','男'),
('stu07', '小石头',26, '180','女'),
('stu08', '小蒋',29, '170','男'),
('stu09', '小贾',20, '174','女');

-- 显示为男性的数据
select * from union_data where stu_set = '男';

-- 显示为女性的数据
select * from union_data where stu_set = '女';

-- 使用联合查询
(select * from union_data where stu_set = '男')
union
(select * from union_data where stu_set = '女');

-- 男性为升序 女生为降序
(select * from union_data where stu_set = '男' order by stu_height asc limit 100)
union
(select * from union_data where stu_set = '女' order by stu_height desc limit 1000);