-- 年龄大于18到30的 逻辑与: and
select * from having_data where age >= 18 and age <= 30;

-- 找出性别为女或 年龄小于18  逻辑或: or
select * from having_data where sex = '女' or age <= 18;

-- 找出性别不为女  逻辑非:not
select * from having_data where not sex ='男';