-- 创建数据库
create database day01;
-- 创建数据库
create table having_data(
  id int primary key auto_increment,
  name varchar(20) not null,
  sex enum('男','女','保密') default '保密',
  age tinyint not null default 18
);

-- 插入数据
insert into having_data(name,sex,age) values
('妲己','女',18),
('黄忠','男',80),
('张良','男',23),
('扁鹊','男',12),
('小乔','女',21),
('大乔','女',23);

-- 找到所有数据
select count(*) from having_data group by sex;

-- 大于6的那一组显示
select group_concat(name),count(*) from having_data group by sex;

select group_concat(name),count(*) from having_data group by sex having count(*) >= 6;