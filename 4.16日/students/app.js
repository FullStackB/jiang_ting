// 导入express框架
const express = require('express');
// 创建服务器
const app = express();
// 导入bodyParser
const bodyParser = require('body-parser'); 
app.use(bodyParser.urlencoded({extended:false}));

// 配置渲染环境
app.engine('html',require('express-art-template'));
app.set('views','views');

// 托管静态目录
app.use(express.static('./public'));
// 导入路由
const router = require('./router');
app.use(router);

// 端口
app.listen(80,() => {
  console.log('http://127.0.0.1');
})