// 导入express 框架
const express = require('express');
// 使用router方法
const router = express.Router();
// 导入controller文件夹
const controller = require('../controller');
// 首页显示
router.get('/',controller.index);
// 添加数据
router.post('/creatuser',controller.creatuser);
// 查询
router.get('/show',controller.show);
// 删除
router.get('/deluser',controller.deluser);
// 暴露出去
module.exports = router;