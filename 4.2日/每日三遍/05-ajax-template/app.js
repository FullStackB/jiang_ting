// 导入express框架
const express = require('express');
// 创建服务器
const app = express();
// 导入body-parser包
const bodyParser = require('body-parser');
// 设置bodyParser包
app.use(bodyParser.urlencoded({extended:false}));
// 配置渲染环境
app.engine('html',require('express-art-template'));
app.set('views','views');
// 配置路由
const router = require('./router');
app.use(router);
// 静态管理
app.use(express.static('./public'));
// 端口监听
app.listen(3044,()=>{
  console.log('http://127.0.0.1:3044');
})