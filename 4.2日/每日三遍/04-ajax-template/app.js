const express = require('express');
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));
app.engine('html',require('express-art-template'));
app.set('views','views');
app.use(express.static('public'));
const router = require('./router');
app.use(router);
app.listen(3040,()=>{
  console.log('http://127.0.0.1:3040');
})