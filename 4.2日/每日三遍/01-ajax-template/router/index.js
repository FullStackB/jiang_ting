const express = require('express');
const router = express.Router();
const controller = require('../controller');
router.get('/',controller.index);
router.post('/createUser',controller.createUser);
router.get('/show',controller.show);
router.get('/deluser',controller.deluser);
module.exports = router;