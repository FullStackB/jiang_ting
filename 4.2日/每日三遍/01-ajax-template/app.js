const express = require('express');
const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));

app.use(express.static('./public'));

app.engine('html',require('express-art-template'));
app.set('views','views');

const router = require('./router');
app.use(router);

app.listen(80,()=>{
  console.log('http://127.0.0.1');
})