// 导入express框架
const express = require('express');
const router = express.Router();
// 导入处理函数的文件夹
const controller = require('../controller');
// 首页显示
router.get('/',controller.index);
// 添加
router.post('/creatuser',controller.creatuser);
// 查询
router.get('/show',controller.show);
// 删除
router.get('/deluser',controller.deluser);
// 暴露
module.exports= router;