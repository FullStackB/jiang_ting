// 导入数据库包
const mysql = require('mysql');
const connection = mysql.createConnection({
  // 地址
  host: '127.0.0.1',
  // 端口
  port: '3306',
  // 账号
  user: 'root',
  // 密码
  password: '123456',
  // 数据库名称
  database: 'jt'
})
// 暴露
module.exports = connection;