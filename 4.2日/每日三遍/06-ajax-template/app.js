// 导入express框架
const express = require('express');
// 创建服务器
const app = express();

// 导入body-parser
const bodyParser = require('body-parser');
// 设置
app.use(bodyParser.urlencoded({extended:false}));

// 配置渲染环境
app.engine('html',require('express-art-template'));
// 配置渲染路径
app.set('views','views');

// 设置托管静态资源
app.use(express.static('./public'));

// 导入路径
const router = require('./router');
app.use(router);

// 端口监听
app.listen(80,()=>{
  console.log('http://127.0.0.1');
})  