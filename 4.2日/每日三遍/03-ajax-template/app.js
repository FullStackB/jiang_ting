// 导入express框架
const express = require('express');
// 创建服务器
const app = express();
// 导入body-parser包
const bodyParser = require('body-parser');
// 配置body-parser
app.use(bodyParser.urlencoded({extended:false}));
// 配置渲染环境
app.engine('html',require('express-art-template'));
// 渲染路径
app.set('views','views');
// 托管静态资源
app.use(express.static('./public'));
// 导入路由
const router = require('./router');
// 设置
app.use(router);

// 端口监听
app.listen(80,()=>{
  console.log('http://127.0.0.1');
})