// 导入express框架
const express = require('express');
// 使用router方法
const router = express.Router();
// 导入controller
const controller = require('../controller');
// 首页显示
router.get('/',controller.index);
// 添加
router.post('/creatUser',controller.creatUser);
// 查询
router.get('/show',controller.show);
// 删除
router.get('/deluser',controller.deluser);
// 暴露
module.exports = router;