// 引入模块http
const http = require('http');
// 创建服务器
const server = http.createServer();
// 监听请求
server.on('request',(req,res)=>{
  // 防止中文乱码
  res.writeHead(200,{'Content-Type':'text/plain;charset=UTF-8'});
  res.end('Hello,传智专修学院')
})
// 监听端口(接听)
server.listen(8899,() => {
  console.log('请访问:http://127.0.0.1:8899');
})
