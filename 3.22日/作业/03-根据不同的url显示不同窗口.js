// 导入http
const http = require('http');
// 创建服务器
const server = http.createServer();
const fs = require('fs');
const path = require('path');
// 监听请求
server.on('request', (req, res) => {
  res.writeHead(200, {
    'Content-Type': 'text/html;charset=UTF-8'
  });
  // 判断路径
  if (req.url === '/' || req.url == '/03-home.html') {
    // 读取当前路径下的html内容
    fs.readFile(path.join(__dirname, './03-home.html'), 'utf8', (err, data) => {
      if (err) {
        console.log(err);
      }
      res.end(data);
    })
    // 判断路径
  } else if (req.url === '/03-study.html') {
    // 读取当前路径下的html内容
    fs.readFile(path.join(__dirname, './03-study.html'), 'utf8', (err, data) => {
      if (err) {
        console.log(err);
      }
      res.end(data);
    })
    // 判断路径
  } else if (req.url === '/03-us.html') {
    // 读取当前路径下的html内容
    fs.readFile(path.join(__dirname, './03-us.html'), 'utf8', (err, data) => {
      if (err) {
        console.log(err);
      }
      res.end(data);
    })
  }
})

// 监听端口 8899
server.listen(8899, () => {
  console.log('请访问:http://127.0.0.1:8899')
})