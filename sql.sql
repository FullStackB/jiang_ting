-- 进入数据库
mysql -uroot -p123456

-- 创建数据库
create database stu;

-- 选择数据库
use stu;

-- 创建班级表
create table class(
  class_id int auto_increment comment '班级id 自动增长',
  class_name VARCHAR not null comment '班级名字',
  PRIMARY KEY(class_id) comment '主键'
);

-- 创建学生表
create table students(
  stu_id int auto_increment comment '学生id',
  stu_name VARCHAR(5) not null comment '学生姓名',
  password VARCHAR(12) not null comment '学生登录密码',
  class_id int,
  PRIMARY KEY(stu_id) comment '主键',
  FOREIGN KEY (class_id) REFERENCES class(class_id)
);

-- 目标表
create table target(
  target_id int auto_increment comment '目标表id',
  target_name varchar(255) not null comment '目标点',
  target_type VARCHAR(10) not null comment '目标类型',
  target_classification varchar(10) not null comment '目标分类',
  primary key(target_id)
);

ALTER TABLE `target` DROP is_OK;
ALTER TABLE `target` DROP is_audit;

-- 学生_目标表
create table student_target(
  student_id int,
  target_id int,
  is_OK TINYINT unsigned not null comment '是否掌握, 0 未掌握 , 1 已掌握',
  is_audit TINYINT unsigned not null comment '是否审核, 0 未审核 , 1 已审核',
  FOREIGN KEY(student_id) REFERENCES students(stu_id),
  FOREIGN KEY(target_id) REFERENCES target(target_id)
)

-- 习题表
create TABLE problem(
  problem_id int auto_increment comment '习题id',
  problem_name VARCHAR(255) not null comment '习题名',
  problem_type VARCHAR(10) not null comment '习题类型',
  target_id int,
  primary key(problem_id),
  FOREIGN key (target_id) REFERENCES target(target_id)
);

-- 日报表
create table daily(
  daily_id int auto_increment comment '日报id',
  daily_plan varchar(255) not null comment '日报计划',
  daily_conclusion VARCHAR(255) not null comment '每日计划',
  daily_date timestamp not null comment '日报日期',
  stu_id int,
  PRIMARY KEY(daily_id),
  FOREIGN KEY(stu_id) REFERENCES students(stu_id) 
);

-- 阶段表
create table phase(
  phase_id int auto_increment comment '阶段id',
  phase_name varchar(10) not null comment '阶段名',
  phase_introduce varchar(50) comment '阶段介绍',
  primary KEY(phase_id)
);

-- 老师表
create table teacher(
  teacher_id int auto_increment comment '老师id',
  teacher_name varchar(10) not null comment '老师姓名',
  password char(12) not null comment '老师登录密码',
  PRIMARY key(teacher_id)
);

