-- 视图的本质是SQL指令（select语句）
-- 基本语法：create view 视图名字 as select指令;	//可以是单表数据，也可以是连接查询，联合查询或者子查询

create view stu_info_v as select * from stu_info;

 create view ckx as select * from stu_info;

-- 使用视图
-- 基本语法：select 字段列表 from 视图名字 [子句];

select info_name from stu_info_v;

select * from stu_info_v;


-- 修改视图：本质是修改视图对应的查询语句
-- 基本语法：alter view 视图名字 as 新select指令;
alter view stu_info_v as select info_age from stu_info;


-- 删除视图
-- drop view 视图的名字
drop view stu_info_v;

-- 查看视图
-- desc 视图名字;
desc stu_info_v;
-- show create view 视图名字
show create view stu_info_v;


mysql> create view ckx as select * from stu_info;
Query OK, 0 rows affected (0.01 sec)

mysql> select info_name from ckx;
+-----------+
| info_name |
+-----------+
| 郭嘉      |
| 孔明      |
+-----------+
2 rows in set (0.01 sec)

mysql> alter view  ckx as select info_age from stu_info;
Query OK, 0 rows affected (0.01 sec)

mysql> drop view ckx;
Query OK, 0 rows affected (0.00 sec)

mysql> desc ckx;
ERROR 1146 (42S02): Table 'day08_b.ckx' doesn't exist
mysql> create view ckx as select * from stu_info;
Query OK, 0 rows affected (0.01 sec)

mysql> desc ckx;
+-----------+-------------+------+-----+---------+-------+
| Field     | Type        | Null | Key | Default | Extra |
+-----------+-------------+------+-----+---------+-------+
| info_id   | int(11)     | NO   |     | 0       |       |
| info_name | varchar(10) | NO   |     | NULL    |       |
| info_age  | tinyint(4)  | YES  |     | 12      |       |
| class_id  | int(11)     | YES  |     | NULL    |       |
+-----------+-------------+------+-----+---------+-------+
4 rows in set (0.02 sec)
