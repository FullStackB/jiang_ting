-- 左边的客户端 进行数据的插入 insert into stu_info values(null, '高源', 19, 1);
-- 右边的客户端 进行数据的查询

-- 以上两个过程是自动做的
-- 自动手动的开关 show variables like 'autocommit';
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| autocommit    | ON 开 |
+---------------+-------+

autocommit 自动提交
+---------------+-------+
| Variable_name | Value |
+---------------+-------+
| autocommit    | off 关 |
+---------------+-------+
-- 手动的下面再说



-- 回滚:
-- 我准备给蒋婷10万 
-- 我点击转账的时候  网断了 这时候 innodb需要把我转出去的钱 给我回滚回来

-- 回滚点
-- 设置回滚点: savepoint 回滚点的名字
-- 回到回滚点: rollback to 回滚点的名字


-- 需求: 第一次修改郭嘉的年龄为30

        -- 设置回滚点

        -- 第二次修改郭嘉的年龄为40 假设我设置错了

        -- 回滚到 之前设置的回滚点 然后提交 30

        -- ====================================================================
        -- 回滚点
-- 设置回滚点: savepoint 回滚点的名字
-- 回到回滚点: rollback to 回滚点的名字
-- ===============================================================================================

-- 回滚点
-- 设置回滚点: savepoint 回滚点的名字
-- 回到回滚点: rollback to 回滚点的名字