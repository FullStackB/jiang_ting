-- 学生和班级

-- 1.创建一个数据库
create database day08_b;
-- 2.使用数据库
use day08_b;
-- 3.创建班级表
create table stu_class (
  class_id int primary key auto_increment,
  class_name varchar(20)
);

create table stu_class(
  class_id int primary key auto_increment,
  class_name varchar(20)
);

create table stu_class(
  class_id int primary key auto_increment,
  class_name varchar(20)
);


-- 4.创建学生表
create table stu_info (
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint default 18,
  class_id int,
  foreign key(class_id) references stu_class(class_id)
);

create table stu_info(
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint default 12,
  class_id int,
  foreign key (class_id)references stu_class(class_id)
);

create table stu_info(
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint default 12,
  class_id int,
  foreign key (class_id ) references stu_class(class_id)
);



insert into stu_class values
(null,'全栈应用开发');

insert into stu_info values
(null,'郭嘉', 32, 1);

insert into stu_info values
(null,'孔明', 28, 1);

-- 增加外键(表后增加)
-- 基本语法： Alter table 从表 add [constraint `外键名`] foreign key(外键字段) references 主表(主键);
alter table stu_info add constraint `classId` foreign key(class_id) references stu_class(class_id);
-- 查看外键 就是在查看表的创建过成
show create table stu_info;

-- 删除外键/修改外键(先删后加叫修改)
-- 外键不允许修改，只能先删除后增加
-- 基本语法：alter table 从表 drop foreign key 外键名字;

alter table stu_info drop foreign key `stu_info_ibfk_1`;

-- =============================================================================================
-- 增加外键（表后增加）
-- 基本语法：Alter table 从表 add[constraint `外键名`] forenign key (外键字段) references 主表（主键）
alter table stu_info add constreint ` ckx` foreign key (class_id) references stu_class(class_id);
-- 查看外键 
show create table stu_info;
-- 删除外键
-- 基本语法：alter table 从表 drop foreign key `外键名字`;
alter table stu_info drop foregin key `ckx`;


-- =============================================================================================
-- 增加外键（表后增加）
-- 基本语法：Alter table 从表 add[constraint `外键名`] forenign key (外键字段) references 主表（主键）
alter table stu_info add constreint ` ckx` foreign key (class_id) references stu_class(class_id);
-- 查看外键 
show create table stu_info;
-- 删除外键
-- 基本语法：alter table 从表 drop foreign key `外键名字`;
alter table stu_info drop foregin key `ckx`;