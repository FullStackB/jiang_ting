-- 将权限分配给指定的用户
-- 语法: grant 权限列表 on 数据库/*.表名/* to 用户;

-- 权限列表：使用逗号分隔，但是可以使用all privileges代表全部权限
-- 数据库.表名：可以是单表（数据库名字.表名），可以是具体某个数据库（数据库.*），也可以整库（*.*）

-- 0.登陆 root用户 删除 wanlum用户
drop user 'wanlum'@'localhost';

-- 1.创建一个用户  wanlum
create user 'wanlum'@'localhost'  identified by 'wanlum';


create user 'ckx'@'localhost' identified by 'ckx';

-- 2.登陆root用户  因为只有root用户可以给指定的用户分配权限
基本语法：grant 权限列表 on 数据库/*.表名/* to 用户;

grant select on mysql.user to 'wanlum'@'localhost';

 grant select on mysql.user to 'ckx'@'localhost';

-- 权限回收：将权限从用户手中收回
-- 基本语法：revoke 权限列表/all privileges on 数据库/*.表/* from 用户;

revoke select  on mysql.user from 'wanlum'@'localhost';

revoke select  on mysql.user from 'ckx'@'localhost';
-- https://www.cnblogs.com/Richardzhu/p/3318595.html 权限表
