-- 创建数据库
create database day_03;
-- 使用数据库
use day_03;
-- 创建班级表
create table stu_class (
  class_id int primary key auto_increment,
  class_name varchar(20)
);
-- 4.创建学生表
create table stu_info (
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint default 18,
  class_id int,
  foreign key(class_id) references stu_class(class_id)
);
-- 插入数据
insert into stu_class values
(null,'全栈应用开发');

insert into stu_info values
(null,'郭嘉', 32, 1);

insert into stu_info values
(null,'孔明', 28, 1);

-- 增加外键（表后增加）
alter table stu_info add constraint `classId` foreign key(class_id) references stu_class(class_id);
-- 查看
show create table stu_info;


-- 删除
alter table stu_info drop foreign key `stu_info_ibfk_1`; 