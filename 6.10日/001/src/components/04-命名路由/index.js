import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import app from './app.vue';
import movieList from './movieList.vue'
import movieDate from './movieDetail.vue'
const router = new VueRouter({
  routes:[
    {path:'/', redirect:'/movie/list'},
    {path:'/movie/list',component:movieList},
    {name:'moviePage',path:'/movie/dete/:id',component:movieDate,props:true}
  ]
})

new Vue({
  render: h=>h(app),
  router
}).$mount('#app')