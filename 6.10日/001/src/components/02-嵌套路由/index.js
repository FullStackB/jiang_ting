import Vue from 'vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import app from './app.vue';
import home from './home.vue';
import about from './about.vue';
import movie from './movie.vue';
import inner from './tabs/inner.vue';
import outer from './tabs/outer.vue';

const router = new VueRouter({
  routes:[
    {path:'/',redirect:'/home'},
    {path:'/home',component:home},
    {path:'/about',component:about},
    {path:'/movie',component:movie,redirect:'/movie/inner',
    children:[
      {path:'/movie/inner',component:inner},
      {path:'/movie/outer',component:outer}
    ]
  }

  ]
})

new Vue({
  render: h => h(app),
  router
}).$mount('#app')