const fs = require('fs');
// 写入内容 writeFileSync
fs.writeFileSync(__dirname + '/03-copy.txt','我准备拷贝信息','utf8',(err) => {
  console.log(err);
})
// 追加 appendFileSync
fs.appendFileSync(__dirname + '/03-copy.txt','我继续追加信息到文件中','utf8',(err) => {
  console.log(err);
})

// 获取 statSync
// 获取文件大小
console.log(fs.statSync('03-copy.txt').size);
// 获取创建时间
console.log(fs.statSync('03-copy.txt').atime);
// 判断是否是一个文件
console.log(fs.statSync('03-copy.txt').isFile());
// 判断是不是目录
console.log(fs.statSync('03-copy.txt').isDirectory());

// 拷贝 copyFileSync
fs.copyFileSync(__dirname + '/03-copy.txt',__dirname + '/03-copyFile.txt','utf8',(err) => {
  console.log(err);
})
