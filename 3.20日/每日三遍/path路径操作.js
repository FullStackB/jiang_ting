// 第一遍
// const path = require('path');
// console.log(path.sep); // 返回当前系统的片段符
// var str = 'C:/Users/蒋婷/Desktop/jiang_ting/3.20日/path路径操作.js'
// console.log(path.dirname(str)); // 返回某个文件夹所在目录
// console.log(path.extname(str)); //扩展名后缀
// // console.log(__dirname + './file/成绩.txt');
// console.log(path.join(__dirname, './file/成绩.txt')); // join拼接路径
// console.log(path.join('f:', "/a", "/b", "/d/a.txt"));


// 第二遍
// const path = require('path');
// console.log(path.sep); 
// var str = 'C:/Users/蒋婷/Desktop/jiang_ting/3.20日/path路径操作.js'
// console.log(path.dirname(str)); 
// console.log(path.extname(str));
// console.log(__dirname + './file/成绩.txt');
// console.log(path.join(__dirname, './file/成绩.txt')); 
// console.log(path.join('f:', "/a", "/b", "/d/a.txt"));


// 第三遍
const path = require('path');
console.log(path.sep); 
var str = 'C:/Users/蒋婷/Desktop/jiang_ting/3.20日/path路径操作.js'
console.log(path.dirname(str)); 
console.log(path.extname(str)); 
console.log(path.join(__dirname, './file/成绩.txt'));
console.log(path.join('f:', "/a", "/b", "/d/a.txt"));