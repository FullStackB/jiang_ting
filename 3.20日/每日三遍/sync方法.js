// 第一遍
// const fs = require('fs');
// const path = require('path');
// console.log(11113)
// fs.readFile(path.join(__dirname,'./file/1.txt'),'utf8',(error,data) => {
//   if(error){
//     console.log(error);
//   }
//   console.log(data);
// })

// console.log(2323);
// fs.readFile(path.join(__dirname,'./file/2.txt'),'utf8',(error,data) => {
//   if(error){
//     console.log(error);
//   }
//   console.log(data);
// })
// console.log(3232);


// console.log('1');
// var result1 = fs.readFileSync(path.join(__dirname,'./file/1.txt'),'utf8');
// console.log(result1);
// var result2 = fs.readFileSync(path.join(__dirname,'./file/2.txt'),'utf8');
// console.log(result2);
// console.log('3');


// 第二遍
// const fs = require('fs');
// const path = require('path');
// console.log(11113)
// fs.readFile(path.join(__dirname,'./file/1.txt'),'utf8',(error,data) => {
//   if(error){
//     console.log(error);
//   }
//   console.log(data);
// })

// console.log(2323);
// fs.readFile(path.join(__dirname,'./file/2.txt'),'utf8',(error,data) => {
//   if(error){
//     console.log(error);
//   }
//   console.log(data);
// })
// console.log(3232);


// console.log('1');
// var result1 = fs.readFileSync(path.join(__dirname,'./file/1.txt'),'utf8');
// console.log(result1);
// var result2 = fs.readFileSync(path.join(__dirname,'./file/2.txt'),'utf8');
// console.log(result2);
// console.log('3');'



// 第三遍
const fs = require('fs');
const path = require('path');
// 异步
console.log(11113)
fs.readFile(path.join(__dirname,'./file/1.txt'),'utf8',(error,data) => {
  if(error){
    console.log(error);
  }
  console.log(data);
})

// 异步
console.log(2323);
fs.readFile(path.join(__dirname,'./file/2.txt'),'utf8',(error,data) => {
  if(error){
    console.log(error);
  }
  console.log(data);
})
console.log(3232);

// 同步
console.log('1');
var result1 = fs.readFileSync(path.join(__dirname,'./file/1.txt'),'utf8');
console.log(result1);
var result2 = fs.readFileSync(path.join(__dirname,'./file/2.txt'),'utf8');
console.log(result2);
console.log('3');