// 第一遍
// const fs = require('fs');
// const path = require('path');
// // console.log(path.join(__dirname,'./file/1.txt'))
// fs.readFile(path.join(__dirname,'./file/1.txt'),'utf8',(error,data) => {
//   if(error) return console.log(error.message)
//   console.log(data);
// })


// 第二遍
// const fs = require('fs');
// const path = require('path');
// fs.readFile(path.join(__dirname,'./file/1.txt') , 'utf8',(err,data) => {
//   if(err) return console.log(err.message);
//   console.log(data);
// })


// 第三遍
const fs = require('fs');
const path = require('path');
// 读取
fs.readFile(path.join(__dirname, './file/1.txt'), 'utf8', (err, data) => {
  if (err) return console.log(err.message);
  console.log(data);
})