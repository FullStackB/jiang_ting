// 第一遍
// const fs = require('fs');
// fs.readFile(__dirname + '/file/成绩.txt','utf8',(error,data)=>{
//   if(error){
//     console.log(error);
//   }

//   var score = data.split(' ');
//   var newArr = [];
//   score.forEach((item) =>{
//     if(item.length > 0){
//       var res = item.replace('=',':');
//       newArr.push(res);
//     }
//   })
//   var result = newArr.join('\n');
//   fs.writeFile(__dirname + '/file/result.txt',result,'utf8',(error) => {
//     if(error){
//       console.log('失败');
//       return 0;
//     }
//     console.log('成功');
//   })
// })

// 第二遍
// const fs = require('fs');
// fs.readFile(__dirname + '/file/成绩.txt','utf8',(error,data) => {
//   if(error){
//     console.log(error);
//   }
//   var score = data.split(' ');
//   // console.log(score);
//   var newArr = [];
//   score.forEach((item) => {
//     if(item.length > 0){
//       var res = item.replace('=',':');
//       newArr.push(res);
//     }
//   })
//   var result = newArr.join('\n');
//   fs.writeFile(__dirname + '/file/result.txt',result,'utf8',(error) => {
//     if(error){
//       console.log('失败');
//       return 0;
//     }
//     console.log('成功');
//   })
// })

// 第三遍
const fs = require('fs');
// 读取内容
fs.readFile(__dirname + '/file/成绩.txt', 'utf8', (error, data) => {
  if (error) {
    console.log(error);
  }
  var score = data.split(' ');
  // 声明一个空数组
  var newArr = [];
  // 遍历成绩
  score.forEach((item) => {
    // 判断
    if (item.length > 0) {
      // 使用replace方法替换
      var res = item.replace('=', ':');
      // 把替换完的成绩插入到空数组中
      newArr.push(res);
    }
  })

  // 换行
  var result = newArr.join('\n');
  // 写入
  fs.writeFile(__dirname + '/file/result.txt', result, 'utf8', (error) => {
    // 判断
    if (error) {
      console.log('失败');
      return 0;
    }
    console.log('成功');
  })
})