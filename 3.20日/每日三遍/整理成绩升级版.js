// 第一遍
// const fs = require('fs');
// fs.readFile(__dirname + '/file/成绩.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }
//   var score = data.split(' ').filter((item) => {
//     return item.length > 0;
//   })
//   // console.log(score);
//   var newArr = score.map((item) => {
//     return item.replace('=',':');
//   })
//   fs.writeFile(__dirname + '/file/result.txt',newArr.join('\n'),'utf8',(error) => {
//     if(error){
//       console.log('失败');
//       return 0;
//     }
//     console.log('成功')
//   })
// })

// 第二遍
// const fs = require('fs');
// fs.readFile(__dirname + '/file/成绩.txt', 'utf8', (error, data) => {
//   if (error) {
//     console.log(error);
//   }
//   var score = data.split(' ').filter((item) => {
//     return item.length > 0;
//   });
//   var newArr = score.map((item) => {
//     return item.replace('=', ':');
//   })
//   fs.writeFile(__dirname + '/file/result.txt', newArr.join('\n'), 'utf8', (error) => {
//     if (error) {
//       console.log('失败');
//       return 0;
//     }
//     console.log('成功');
//   })
// })


// 第三遍
const fs = require('fs');
// 读取内容
fs.readFile(__dirname + '/file/成绩.txt', 'utf8', (error, data) => {
  if (error) {
    console.log(error);
  }
  // 转换字符串 并遍历判断
  var score = data.split(' ').filter((item) => {
    return item.length > 0
  });
  // 替换=
  var newArr = score.map((item) => {
    return item.replace('=', ':')
  });

  // 写入内容
  fs.writeFile(__dirname + '/file/result.txt', newArr.join('\n'), 'utf8', (error) => {
    if (error) {
      console.log('失败');
      return 0;
    }
    console.log('成功')
  })
})