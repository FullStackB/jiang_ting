-- 登录数据库
mysql -uroot -p123456

-- 创建数据库
create database stu;

-- 选择数据库
use stu;

-- 创建表
-- 学生表
create table student(
  no varchar(20) not null primary key comment '学号',
  name varchar(20) not null comment '学生姓名',
  sex varchar(20) not null comment '学生性别',
  birthday datetime comment '学生出生年月日',
  class varchar(20) comment '学生所在班级'
);
-- 课程表
create table course(
  no varchar(20) primary key comment '课程号',
  name varchar(20) not null comment '课程名称',
  t_no varchar(20) not null comment '教工编号'
);

create table score(
  s_no varchar(20) not null comment '学号',
  c_no varchar(20) not null comment '课程号',
  degree Decimal(4,1) comment '成绩',
  primary key(s_no,c_no)
);

create table teacher(
  no varchar(20) primary key comment '教工编号',
  name varchar(20) not null comment '教工姓名',
  sex varchar(20) not null comment '教工性别',
  birthday datetime comment '教工出生年月',
  prof varchar(20) comment '职称',
  depart varchar(20) not null comment '教工所在部门'
);

-- 插入数据
insert into student values
('101','赵军','男','1987-03-20','95033'),
('103','毛君','男','1984-09-03','95031'),
('105','李明','男','1982-11-02','95031'),
('107','范丽','女','1987-01-23','95033'),
('108','王华','男','1981-09-01','95033'),
('109','张芳','女','1983-01-10','95031');

insert into teacher values
('804','王诚','男','1957-12-02','副教授','计算机系'),
('825','张萍','女','1971-05-05','助教','计算机系'),
('831','毛冰','女','1975-08-14','助教','电子工程系'),
('856','李旭','男','1966-03-12','讲师','电子工程系');

insert into score values
('103','3-105','92'),
('103','3-245','86'),
('103','6-166','85'),
('105','3-105','88'),
('105','3-245','75'),
('105','6-166','79'),
('109','3-105','76'),
('109','3-245','68'),
('109','6-166','81');

insert into course values
('3-105','计算机导论','825'),
('3-245','操作系统','804'),
('6-166','数字电路','856'),
('9-888','高等数学','831');

-- 查询student表中的所有记录的name、sex和class列信息
select name,sex,class from student;

-- 查询教师所有的单位(不重复)的depart列信息

-- 查询score表中成绩在70到90之间的所有记录
select * from score where degree >= 70 and degree <= 90;