-- 删除复合
delete from new where pinyin = 'b' limit 3;

create table del_data(
  id int primary key auto_increment,
  name varchar(4) not null
);

insert into del_data(name) values
('a'),
('b'),
('c'),
('d'),
('e'),
('d');

delete from del_data where name ='a' limit 2;

show create table del_data;
delete from del_data where name ='b' limit 2; 