-- 创建表
create table query_data(
  id int primary key auto_increment,
  name varchar(5) not null
);

-- 插入数据
insert into query_data(name) values
('张三'),
('李四'),
('蔡明'),
('王五'),
('赵六'),
('王五'),
('张三');

-- 查询所有数据(无论重复否)
select * from query_data;
select all * from query_data;

-- 查询非重复
select distinct * from query_data;

create table query_data1(
  pinyin varchar(2)
);

insert into query_data1 values
('a'),
('b'),
('c'),
('b'),
('d');

select distinct * from query_data1;
select distinct pinyin as name1,pinyin as name2 from query_data1;

create table stu(
  id int primary key auto_increment,
  name varchar(6) not null,
  age tinyint not null  default 18,
  sex enum('男','女','保密') default '保密'
);

insert into stu(name,age,sex) values
('小白龙',default, '男'),
('小黑龙',20, '男'),
('小紫龙',21, '女'),
('小红龙',17, '男'),
('小绿龙',19, '女'),
('小粉龙',16, '男'),
('小金龙',24, '女'),
('小龙龙',30, '男'),
('小云龙',43 ,'女');

create table stu_score(
  id int primary key auto_increment,
  name  varchar(6) not null,
  score tinyint not null
);
insert into stu_score(name,score) values
('小白龙',100),
('小黑龙',60),
('小紫龙',70),
('小红龙',90),
('小绿龙',99),
('小粉龙',80),
('小金龙',40),
('小龙龙',30),
('小云龙',20);

select name,sex as gender from stu;

select * from stu, stu_score;

select * from (select name,age from stu ) as info;

select name from (select name,age from stu ) as info;
