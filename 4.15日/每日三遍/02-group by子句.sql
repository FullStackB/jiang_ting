
select * from stu group by sex;

select group_concat(name),sex from stu group by sex;

select count(*) from stu;
select count(*), sum(score) from stu_score;

select max(score) from stu_score;

select min(score) from stu_score;

select avg(score) from stu_score;

select group_concat(name),age,sex from stu_info group by sex,age;
select group_concat(name),age,sex from stu_info group by sex desc,age desc;
select count(*) from stu_infostu_info group by  age desc with rollup;
select id,count(*) from stu_info group by age desc, id desc with rollup;
