create table old(
  pinyin varchar(2)
);

insert into old values('a'),('b'),('c'),('d'),('e');

create table new like old;

insert into new select * from old;