-- 创建表
create table dupilcate_key(
  s_id varchar(5) primary key,
  s_name varchar(5) not null
);

-- 插入一堆数据
insert into dupilcate_key values
('s001','赵一'),
('s002','钱二'),
('s003','孙三');

-- 插入
insert into dupilcate_key values('s003','孙三');

-- 解决主键冲突
insert into dupilcate_key values('s003','熊大') on dupilcate key update s_name = '熊大';
replace into dupilcate_key values('s002','熊大');