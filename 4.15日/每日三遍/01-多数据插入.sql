-- 创建数据库
create database day;

-- 选择数据库
use day;

-- 创建表
create table more(
  name varchar(4)
);

-- 插入数据
insert into more values('张三'),('李四'),('王五');