// 引入
const connection = require('../data');

module.exports.usersShow = (req,res)=>{
    connection.query('select * from users', (err, results) => {
        if (err) return console.log(err);
        res.json(results);
    })
}
// 添加数据
module.exports.usersAdd = (req, res) => {
    let addStr = [req.body.nickname, req.body.username, req.body.email,  req.body.phone, req.body.region,req.body.jurisdiction, req.body.state];
    connection.query('insert into users(nickname,username,email,phone,region,jurisdiction,state) values (?,?,?,?,?,?,?)', addStr, (err, result) => {
        if (err) return console.log(err);
        res.json({
            code: '1'
        })
    })
}
// 回显数据
module.exports.findUser = (req, res) => {
    let userId = req.query.id;
    connection.query('select * from users where id=' + userId, (err, result) => {
        if (err) return console.log(err);
        res.json(result[0])
    })
}
// 更新用户信息
module.exports.updateUser = (req, res) => {
    let params = [req.body.nickname, req.body.username, req.body.email,  req.body.phone, req.body.region,req.body.jurisdiction, req.body.id];
    connection.query('update users set nickname=?,username=?,email=?,phone=?,region=?,jurisdiction=? where id=?', params, (err, result) => {
        if (err) return console.log(err);
        res.json({
            code: '2'
        })
    })
}
// 删除
module.exports.usersDelete = (req, res) => {
    connection.query('delete from users where id=?', req.query.id, (error, results) => {
        if (error) return console.log(error);
        if (results.affectedRows) {
            res.json({
                code: '0'
            })
        }
    })
}
// 搜索
module.exports.searchUser = (req, res) => {
    connection.query('select * from users where nickname like "%' + req.body.searchVal + '%"', (err, result) => {
        if (err) return console.log(err);
        res.json(result);
    })
}