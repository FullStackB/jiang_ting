const express = require('express');
const app = express();
// 额外服务
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));
// 配置模板引擎
app.engine('html', require('express-art-template'));
// 静态资源
app.use(express.static('public'));
// 路由
const route = require('./route');
app.use(route);

app.listen(80, function () {
    console.log('http://127.0.0.1');
})