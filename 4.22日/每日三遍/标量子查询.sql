mysql -uroot -p123456

create database day_01;

use day01;

create table stu_class(
  class_id int primary key auto_increment,
  class_name varchar(20) not null
);

create table stu_info(
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint unsigned,
  class_id int
);

insert into stu_class values
(null, 'Java应用方向'),
(null, '全栈应用方向'),
(null, 'python应用方向'),
(null, '大数据应用方向');

insert into stu_info values
(null, '小鱼儿', 30, 1),
(null, '段誉', 18, 2),
(null, '木婉清', 16, 3),
(null, '虚竹', 23, 4),
(null, '乔峰', 32, 1),
(null, '黄眉僧', 50, 2),
(null, '扫地僧', 60, 3),
(null, '钟万仇', 44, 4),
(null, '云中鹤', 30, 1),
(null, '萧远山', 56, 2),
(null, '萧炎', 24, 3),
(null, '西门庆', 26, 4);

-- 我知道某一个学生的姓名 想要求出该学生的班级名称
select class_name from stu_class where class_id = (select class_id from stu_info where info_name = "西门庆");

select class_name from stu_class where class_id = (select class_id from stu_info where info_name = '西门庆');

select class_name from  stu_class where class_id = (select class_id from stu_info where info_name = '扫地僧');