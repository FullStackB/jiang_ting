
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
// 导出配置
module.exports = {
  // 设置打包模式
  mode: 'development',
  // 设置入口文件
  entry: './src/js/index.js',
  // 输出文件
  output: {
    // 输出文件的路径
    path: path.join(__dirname, './dist/'),
    // 输出文件的名字
    filename: 'index.js'
  },
  // 配置devserver
  devServer: {
    contentBase: './dist'
  },
  // 因为文件打包了 我也不知道我自己写的文件错误到底在哪个文件的哪一行
  // devtool: 'inline-source-map',
  module: {
    rules: [
      // 这里是各种webpack干不了的事情 让各种loader来做

      // 处理css
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      // 处理图片 尽量不要算svg 不是说svg不是图片 但是现在子图文件中有svg
      // 当limit的值等于5000或大于5000以上用的是url引入
      // 当limit的值小于5000 用的是base64引入
      { test: /\.(png|bmp|jpg|gif)$/, use: 'url-loader?limit=5000' }
    ]
  },
  plugins: [
    // 自动清理dist目录
    new CleanWebpackPlugin(),
    // 自动注入js
    new HtmlWebpackPlugin({
      // 告诉webpack html模板的位置
      template: './src/index.html',
      // 输出到dist中的文件名字
      filename: 'index.html'
    })
  ]
}