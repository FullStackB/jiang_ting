import Vue from 'vue/dist/vue';

Vue.component('my-com', {
  template: `
  <div>
    <h1>{{h1msg}}</h1>
  <div>
    <h1>{{divmsg}}</h1>
  </div>
 </div>
  `,
  data:function(){
    return{
      h1msg:'我是全局组件',
      divmsg:'我是div包裹的h1'
    }
  }
})
const vm = new Vue({
  el:'#app'
})