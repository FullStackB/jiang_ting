// 导入path
const path = require('path');
// 引入自注入的包
const HtmlWebpackPlugin = require('html-webpack-plugin');
// 实例化 配置
const htmlWebpackPlugin = new HtmlWebpackPlugin({
  // 自注入文件路径
  template:'./src/index.html',
  filename:'index.html'
});
//导入热更新的插件
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
// 实例化 
const cleanWebpackPlugin = new CleanWebpackPlugin();
// 导入插件
const VueLoaderPlugin = require('vue-loader/lib/plugin')
// new 一个插件的实例对象
const vuePlugin = new VueLoaderPlugin()

// 配置webpack
module.exports = {
  // 压缩模式
  mode:'development',
  // 入口文件
  entry:'./src/js/index.js',
  // 配置输出文件
  output:{
    // 输出文件路径
    path:path.join(__dirname,'./dist/'),
    // 输出文件名
    filename:'index.js'
  },
  // 配置loader
  module:{
    rules:[
      // 处理css的loader
      {test: /\.css$/,use: ['style-loader','css-loader']},
      // 处理图片的css
      {test: /\.(png|svg|jpg|gif)$/,use: ['file-loader']},
      // 处理vue文件
      { test: /\.vue$/, use: 'vue-loader' }
    ]
  },
  // 配置插件
  plugins:[htmlWebpackPlugin,cleanWebpackPlugin,vuePlugin]
}