const path = require('path');
const HtmlPlugin = require('html-webpack-plugin');
const htmlPlugin = new HtmlPlugin({
  template: './src/index.html',
  filename: 'index.html'
})
// 导入插件
const VueLoaderPlugin = require('vue-loader/lib/plugin')
// new 一个插件的实例对象
const vuePlugin = new VueLoaderPlugin();



module.exports = {
  mode: 'development',
  entry: './src/js/index.js',
  output: {
    path: path.join(__dirname, './dist'),
    filename: 'main.js'
  },
  module: {
    rules: [
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      {test: /\.(png|svg|jpg|gif)$/,use: ['file-loader']},
      { test: /\.vue$/, use: 'vue-loader' }
    ]
  },
  plugins: [htmlPlugin,vuePlugin]
}