import Vue from 'vue/dist/vue';
Vue.component('my-count',{
  template:`
    <div>
      <button @click="handAdd">+1</button>
      <h3>当前值是{{count}}</h3>
    </div>
  `,
  data:function(){
    return{
      count:0
    }
  },
  methods:{
    handAdd:function(){
      this.count++;
    }
  }
})
const vm = new  Vue({
  el:'#app1'
})
const vm2 = new  Vue({
  el:'#app2'
})