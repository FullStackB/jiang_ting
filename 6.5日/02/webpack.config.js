const path = require('path');
const HtmlPlugin = require('html-webpack-plugin');
const htmlPlugin = new HtmlPlugin({
  template: './src/index.html',
  filename: 'index.html'
})

module.exports = {
  mode: 'development',
  entry: './src/js/index.js',
  output: {
    path: path.join(__dirname, './dist'),
    filename: 'main.js'
  },
  module: {
    rules: [
      { test: /\.css$/, use: ['style-loader', 'css-loader'] },
      {test: /\.(png|svg|jpg|gif)$/,use: ['file-loader']}
    ]
  },
  plugins: [htmlPlugin]
}