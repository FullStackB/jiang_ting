import Vue from 'vue/dist/vue';
Vue.component('my-count',{
  template:`
    <div>
      <button @click="handAdd">+1</button>
      <h3>当前值是{{count}}</h3>
    </div>
  `,
  data:function(){
    return{
      count:0
    }
  },
  methods:{
    handAdd:function(){
      this.count++;
    }
  }
})
const vm = new  Vue({
  el:'#app1'
})
const vm2 = new  Vue({
  el:'#app2',
  components:{
    "my-pri":{
      template:`
      <div class="nav">
      <ul>
        <li v-for="(item, index) in nameList" :key="item.id">{{item.name}}</li>
      </ul>
    </div>
      `,
      data:function(){
        return{
          nameList:[
            {
              id: 1,
              name: "张三丰"
            },
            {
              id: 2,
              name: "张翠山"
            },
            {
              id: 3,
              name: "张奋强"
            },
            {
              id: 4,
              name: "张无忌"
            },
            {
              id: 5,
              name: "张冰雪"
            }
          ]
        }
      }
    }
  }
})