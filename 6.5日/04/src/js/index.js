import Vue from 'vue/dist/vue';
Vue.component('my-com',{
  template:`
  <div>
    <h1 @click="handleClick">{{h1Msg}}</h1>
    <div>
      <h1>{{divMsg}}</h1>
    </div>
  </div>
  `,
  data:function(){
    return{
      h1Msg:'我是全局组件',
      divMsg:'我是div包裹的h1'
    }
  },
  methods:{
    handleClick:function(){
      console.log('我是点击之后的效果')
    }
  },
  created:function(){
    console.log(this.h1Msg);
    console.log('我是组件声明的周期函数created');
  }
})
const vm = new  Vue({
  el:'#app'
})