// 引入vue
import Vue from 'vue';

// 引入包
import VueRouter from 'vue-router';
// 配置包
Vue.use(VueRouter);

import app from './app.vue';
import login from './login.vue';

// 实例化
const router = new VueRouter({
  routes:[
    {path:'/',redirect:'/login'},
    {path:'/login',component:login}
  ]
})

// 实例化
const vm = new Vue({
  // 模板id
  el:'#app',
  // 渲染
  render: h => h(app),
  // 挂载路由
  router
})