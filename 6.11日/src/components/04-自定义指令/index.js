import Vue from 'vue';

import app from './app.vue';

Vue.directive('red',{
  bind:function(el){
    el.style.color = 'red';
  }
})


Vue.directive('color',{
  bind:function(el,binding){
    el.style.color = binding.value
  }
})


Vue.directive('focus',{
  bind:function(el,binding){
    el.focus();
    console.log(el.parentNode)
  },
  inserted:function(el){
    el.focus();
    console.log(el.parentNode +"aaa")
  },
})

const vm = new Vue({
  el:'#app',
  render: h => h(app)
})