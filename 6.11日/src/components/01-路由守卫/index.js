import Vue from 'vue';

import VueRouter from 'vue-router';

Vue.use(VueRouter);

import app from './app.vue';
import home from './home.vue';
import login from './login.vue';

const router = new VueRouter({
  routes: [
    {path:'/',redirect:'/home'},
    {path:'/home',component:home},
    {path:'/login',component:login}
  ]
})

router.beforeEach((to,from,next) => {
 if(to.path == '/login') return next();
 
 let tokenStr = window.sessionStorage.getItem('token');
 if(!tokenStr) return next('/login');
 next();
})

const vm = new Vue({
  el: '#app',
  render: h => h(app),
  router
})